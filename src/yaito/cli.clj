(ns yaito.cli
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:import net.veerkesto.PrefixTree)
  (:import kotlin.Pair)
  (:import net.veerkesto.Yaito)
  (:use clojopts.core)
  (:gen-class))

(defn make-prefix-tree [words]
  (->> words
       (map (fn [word] (Pair. word true)))
       (into-array Pair)
       PrefixTree.))

(defn read-dict [uri]
  (let [words (->> (slurp uri)
                     (str/split-lines)
                     (sort))]
    (into-array String words)))

(defn read-default-dict [lang]
  (let [lang-files {"khmer" "khmerwords.txt"
                    "lao" "laowords.txt"
                    "thai" "tdict-std.txt"}]
    (read-dict (io/resource (get lang-files lang "thai")))))

(defn -main [& args]
  (let [opts (clojopts "yaito"
                       args
                       (with-arg lang l "language")
                       (with-arg dix-path p "dictionary path"))
        lang (get opts :lang "thai")
        dix-path (get opts :dix-path)
        dix (if dix-path
              (read-dict dix-path)
              (read-default-dict lang))
        tokenizer (Yaito. dix)]
    (doseq [line (line-seq (io/reader *in*))]
      (println (str/join "|" (.tokenize tokenizer line))))))
