# yaito-cli

yaito-cli is command line tool and word list for yaito - a word tokenizer for ASEAN languages


# Usage

## Prepare

     lein uberjar

## Default

     java -jar target/uberjar/yaito-cli-1.0-standalone.jar

## With specific external dictionary

     java -jar target/uberjar/yaito-cli-1.0-standalone.jar -p <dictionary path>

## With specific language

     java -jar target/uberjar/yaito-cli-1.0-standalone.jar -l <language i.e. lao/thai/khmer>

